#!/bin/bash

function build_version() {
  local docker_version="$1"
  local app_version="==$2"

  if [[ -z "$app_version" ]]; then
    arg_version=""
  fi

  docker build "--build-arg=$arg_version" --tag "chrispjay/subs2cia:$docker_version" .
}

cat versions.txt | while read version; do
  build_version "$version" "$version"
done
build_version "latest"
